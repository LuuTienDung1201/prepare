# Chuẩn bị gì **trước** khi tới lớp học Python http://pymi.vn ? 🤔

- Cài đặt Python 3.8 (hoặc mới hơn)
- Tải tài liệu về máy để xem offline
- Tham gia Slack
- Cài đặt các phần mềm cần thiết trước buổi 3
- Xem chi tiết bên dưới.

## Cài đặt Python
- Nếu dùng Ubuntu: (khuyến khích bản 20.04, tránh các rắc rối không cần thiết), mở terminal rồi gõ
  ```
  sudo apt-get update && sudo apt-get install -y python3-pip
  # rồi nhập password vào và enter
  ```
  Mở `Terminal` rồi gõ `python3` thấy hiện chương trình có giao diện, có dấu `>>>` là được.
- Nếu dùng Windows: cài Python qua Windows store
https://docs.microsoft.com/en-us/windows/python/beginners#install-python
rồi bật `Powershell` và gõ `python`.
- MacOS/Windows: tải và cài đặt Python3 lên máy bản 3.8.2 (https://www.python.org/downloads/release/python-382/) (Windows,OSX/Mac,Linux).
  Lý do sử dụng bản này vì đây là phiên bản có sẵn trên các Ubuntu server 20.04. Sau này khi viết các chương trình Python, các bạn không cần phải lo lắng về phiên bản khi chạy code trên server của mình.
- **Trường hợp xấu nhất** Trên Windows nếu các cách trên không được, hãy tải WinPython (bộ đóng sẵn nhiều gói cần thiết, bản zero cho kích thước nhỏ hơn): https://winpython.github.io/ - [xem hướng dẫn Tiếng Việt](https://medium.com/pymi/l%E1%BA%ADp-tr%C3%ACnh-python-tr%C3%AAn-windows-v%E1%BB%9Bi-winpython-75b6d6c42d1)
  Hay **Anaconda** (https://www.continuum.io/downloads) - Video hướng dẫn: https://www.youtube.com/watch?v=YJC6ldI3hWk
- **Quan trọng** Nếu quyết định dùng Windows, bạn cần đảm bảo: biết mở `Powershell` và chuyển thư mục với lệnh `cd`, bật python từ cmd/powershell.
- Trên Windows, sau khi bật [PowerShell lên (không phải CMD)](https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/01-getting-started?view=powershell-7.1#where-do-i-find-powershell), gõ `py`, thấy kết quả có dấu `>>>` là được.

## Chatting - Slack
- Nhập email vào http://invite.pymi.vn/ để nhận thư mời tham gia forum (Slack) của lớp, mọi thông tin trao đổi thực hiện tại đây. Tạo tài khoản xong nhớ set avatar (hình đại diện). Ngay khi đăng ký trên pymi.vn xong, bạn đã nhận được email mời tham gia, hãy kiểm tra hòm thư với từ khóa Slack.

## Dùng máy tính gì? <a name="computer"></a>
- Máy tính để lập trình Python không cần có cấu hình cao, một máy tính 3->7 triệu
  (mua lại/second hand) là thừa để học/làm rồi. Một chiếc máy tính 30 triệu có
  thể đẹp, hay "oai" nhưng không giúp bạn lập trình giỏi hơn.
- Một chiếc máy tính lập trình nên ưu tiên: RAM (8GB là ngon, 4GB là ổn), ổ [SSD](https://www.familug.org/2017/06/ssd.html),
  pin lâu, không nóng. Còn CPU hay GPU là điều không quan trọng.
- Nếu muốn trở thành lập trình viên chuyên nghiệp, nên cài hệ điều hành Ubuntu 20.04 (https://www.ubuntu.com/download/desktop)
  dùng thay Windows (Google hướng dẫn đầy trên mạng).
  Ubuntu có cài sẵn Python. **Chú ý backup dữ liệu (copy ra chỗ khác: USB, máy khác) trước khi cài Ubuntu, mất đừng kêu 😛)**. Tránh việc dual-boot chung với Windows nếu không thực sự bắt buộc để tránh rắc rối không cần thiết.
  PS: biết code trên Ubuntu +2 triệu tiền lương.
  **KHÔNG CHỌN NGÔN NGỮ TIẾNG VIỆT KHI CÀI ĐẶT - DÙNG TIẾNG ANH**
- Có Macbook lập trình cũng ổn, vì MacOS là một UNIX OS, tương đối
  giống Ubuntu (hay các hệ điều hành nhân Linux) khi sử dụng dòng lệnh.

## Chuẩn bị tài liệu
- Tải sẵn tài liệu ở định dạng HTML https://docs.python.org/3.6/download.html
  giải nén ra và xem trên máy (vào file index.html). Mở sẳn mục "Tutorial" (tương đương với
  xem online tại https://docs.python.org/3/tutorial/index.html). Nếu link die, thử
  lại với link https://docs.python.org/3/download.html
- Đọc http://pymi.vn/tutorial cho quen
- Xem clip tại https://www.youtube.com/playlist?list=PLqVzNKNeM_HyuAfJYN3msXT8a6EpGLjpL
  để không bị lạ lẫm.
- Sẽ không cần sách vở / tài liệu nào khác.
- Nếu bạn lo không đủ tài liệu, có rất nhiều sách ở đây nhưng đều không cần thiết cho quá
  trình học, chỉ để tham khảo - hoặc chỉ có giá trị sau khi đã học xong: https://www.familug.org/2016/12/free-ebook.html

## Cài đặt Ubuntu <a name="ubuntu"></a>

Học viên học Python chỉ để bổ trợ thêm công việc, sau khi học xong vẫn ra dùng Windows/MacOS hàng ngày (và không phải người quyết định - vd công ty cấp máy), thì nên tiếp tục dùng Windows/MacOs, không nhất thiết phải cài Ubuntu - nhưng quá trình học sẽ gặp một số vấn đề rắc rối, nhưng hoàn toàn không phải là điều gì quá khủng khiếp cản trở việc học.

Học viên muốn học Python ra để đi làm Python, kiếm việc mới thì KHUYẾN KHÍCH dùng Ubuntu.

Có 4 lựa chọn để cài Ubuntu khi sử dụng Windows, 2 phương pháp cuối chỉ nên
thực hiện khi có kinh nghiệm/hiểu biết về việc cài đặt hệ điều hành.
Dùng Ubuntu bản 20.04, tránh các rắc rối không cần thiết.

### Cài duy nhất Ubuntu trên máy, xóa bỏ Windows
Cách đơn giản nhất để cài Ubuntu, bạn sẽ bỏ hết dữ liệu và Windows trên máy.
Không dùng Windows từ đây. Tìm hướng dẫn cài Ubuntu đầy dãy trên mạng và làm theo.
Cách này khiến người học thu được kết quả tốt nhất, ít gặp khó khăn nhất.
Chú ý: với học viên có lý do phải dùng Windows thì không nên làm cách này.

### Cài máy ảo Ubuntu chạy trên Windows
Cài phần mềm VirtualBox hay VMWare lên máy Windows, sau đó cài đặt máy ảo Ubuntu.

Cách này an toàn nhất, vẫn giữ lại Windows, không lo mất gì.

Nhược điểm: máy ảo chậm hơn máy thật nhiều lần.

### Bật Ubuntu trên Windows 10 - Linux Subsystem
Vì một lý do không chính đáng nào đó, bạn không chịu cài Ubuntu, thì bạn có thể
xem cách bật hệ thống Ubuntu thử nghiệm trên Windows 10 tại [đây](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/) hay clip https://www.youtube.com/watch?v=DmsJHocTt84&app=desktop

PS: đây là thử nghiệm của Windows 10, có lỗi thì hãy tự tìm cách sửa, chúng tôi
không có kinh nghiệm và không hỗ trợ.

### Dual boot - cài song song Ubuntu với Windows
Cài Ubuntu tồn tại song song với Windows trên máy. Cách này vẫn giữ được Windows,
và được lựa chọn Ubuntu khi cần.

Nhược điểm: cài đặt phức tạp.

## Chuẩn bị trước buổi 3

### Tạo tài khoản GitLab (sau khi đóng học phí)
- Đăng ký một tài khoản tại https://gitlab.com/ để xem bài tập, nộp bài làm,
  nhớ tạo username và password mới, không đăng nhập bằng tài khoản Google, tạo tài khoản xong nhớ set avatar (hình đại diện) - nick GitLab có thể tùy chọn nhưng tên phải là họ tên thật.
- KHÔNG ĐẶT dấu chấm `.` trong username.
- Học viên tham gia vào GitLab group do lớp cung cấp *SAU BUỔI 3* để có quyền xem bài tập.

### Cài ipython
Cài ipython bằng bất cứ cách nào có thể. Sau đó bật lên bằng cách gõ lệnh:
`ipython3` hay `ipython`.

#### Ubuntu

```
sudo apt-get update; sudo apt-get install -y ipython3
```

Rồi gõ `python3 -m IPython` để bật.

#### Windows

```
py -m pip install --upgrade pip
py -m pip install ipython
```

Rồi gõ `py -m IPython` để bật.

![ipython](win_ipython.png)

### Cài git
- Cài đặt git lên máy (https://git-scm.com/downloads). Git là một công cụ
  có giao diện dòng lệnh (gõ các lệnh để làm việc). Nếu vì lý do gì mà không muốn
  gõ lệnh, hãy tự hỏi tại sao muốn gõ code?
- Ubuntu: `sudo apt-get update; sudo apt-get install -y git tig`
  Khi `apt-get` yêu cầu nhập password, `apt-get` sẽ không hiện password hay dấu
  sao nhưng vẫn đang chờ bạn nhập password rồi gõ enter.
- OSX: cài [`homebrew`](https://brew.sh/), sau đó `brew update && brew install git`
- Windows: lên trang chủ tải về và next next next... Cài xong, chuột phải ra
  desktop, chọn "Git bash here".
- Gõ lệnh `git version` thấy hiện ra kết quả là đã cài thành công.
- Học sử dụng git và gitlab: https://try.github.io/ tham khảo https://pymivn.github.io/vinagit/
- Cài git chứ không phải cài `GitHub` hay cài `GitLab`. GitHub, GitLab là
  các trang web cung cấp dịch vụ lưu trữ các thư mục chứa code (repo).
  GitHub phổ biến với cộng đồng mã nguồn mở, GitLab cho phép lưu repo bí mật
  (private) mà không mất tiền, vì vậy khi học ở lớp thì dùng GitLab.

### Cài đặt editor<a name="editor"></a>
Nếu như Microsoft Word là "trình soạn thảo văn bản", thì editor là các
"trình soạn thảo code", trên Windows có sẵn "notepad", nhưng rất đểu, không
khuyến khích. Hướng dẫn chạy code trên Windows https://www.youtube.com/watch?v=UPZvzlfsiaY
PS: clip chạy Python 2, lớp học Python 3.

Cài một editor/IDE tuỳ thích.

- Nếu bật được IDLE thì có thể dùng luôn File > new File - xem [Video](https://www.youtube.com/watch?v=5hwG2gEGzVg)
- Khuyên dùng VSCode: https://code.visualstudio.com/docs/languages/python - đặc biệt với học viên chưa code bao giờ.
- Best editor: KHÔNG TỒN TẠI. Gợi ý: [Sublime Text 3](http://www.sublimetext.com/)
hoặc [VS Code](https://code.visualstudio.com/download) (khác với VisualStudio to nặng).
- Best IDE: Pycharm https://www.jetbrains.com/pycharm/ . No 1, không cần cãi 😎
Nhược điểm: nặng/ ngốn ram. Nếu dùng, chỉ nên dùng từ buổi 7 trở đi - để làm
quen với cách chạy code bằng lệnh trước.
- Chưa đủ phê 😗 Vim (http://www.vim.org/download.php) hoặc Emacs (https://www.gnu.org/software/emacs/download.html).
  Mới bắt đầu có thể dùng http://spacemacs.org/ (emacs) hay https://www.onivim.io/ (vim) cho đầy đủ sẵn, ít phải cài đặt / cấu hình thêm.


## Nếu bạn bỏ lỡ buổi học đầu tiên
Nhớ làm đầy đủ các phần chuẩn bị nói trên (cài python, tải tài liệu).
Buổi đầu lớp học về các kiểu số, boolean, nếu lỡ buổi học có thể đọc các
tài liệu sau để nắm đủ kiến thức.

- http://pymi.vn/tutorial/python-la-gi/
- http://pymi.vn/tutorial/python-integer/
- http://pymi.vn/tutorial/python-calculation-2/
- http://pymi.vn/tutorial/boolean/
- http://pymi.vn/blog/why-not-float/

Tham khảo [bài giảng buổi 1 của khoá
201706](https://gist.github.com/hvnsweeting/fa9b1f43cbac283bb8901123b53c2a2e)

ĐỌC THÊM tài liệu về số:
- https://docs.python.org/3/tutorial/introduction.html#numbers

## Nếu bạn bỏ lỡ buổi học thứ hai
Buổi 2 lớp học về string, list cơ bản. Xem tài liệu sau để nắm đủ kiến thức:
- http://pymi.vn/tutorial/string1/
- http://pymi.vn/tutorial/naming/
- http://pymi.vn/tutorial/unicode/
- Đăng ký một tài khoản Slack tại http://invite.pymi.vn/ để trao đổi / chat
  chit với các đồng môn, nhận thông báo về lớp.

Tham khảo [bài giảng buổi 2 của khoá
201706](https://gist.github.com/hvnsweeting/dee9ed135118c48a764d1793eec669ef)

Đọc thêm tài liệu về string và list:
- https://docs.python.org/3/tutorial/introduction.html#strings
- https://docs.python.org/3/tutorial/introduction.html#lists
